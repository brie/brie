# Hello, world!

My name is **Brie** Carranza. :cat: :unicorn: :sunflower:

The most exciting things I have going here are:

  - 🪞 a mirror of [pastebin-bisque](https://github.com/bbbbbrie/pastebin-bisque) -- the most popular software I have written. 
    - 🚀 It's now available via PyPI with `pip install pastebin-bisque`.
  - :strawberry: [awesome-ldap](https://gitlab.com/brie/awesome-ldap) - a curated list of LDAP resources.
  - :wastebasket: [faux-credential-dump-generator](https://gitlab.com/brie/faux-credential-dump-generator) - Generate a fake credential dump, for testing purposes. 
  - :star: [graphql](https://gitlab.com/brie/graphql) - Notes and scripts and stuff as I learn more about GraphQL. 

Elsewhere:

  - :cheese: [brie.dev](https://brie.dev) - my blog
  - :house: [brie.carranza.engineer](https://brie.carranza.engineer/)
  - :sunflower: [sunflower.gallery](https://sunflower.gallery) - photo gallery
  - 🐘 I am [brie@infosec.exchange](https://infosec.exchange/@brie) on Mastodon. 
    - :bird: [@whoamibrie](https://twitter.com/whoamibrie) on the bird site -> `read only` mode
  - :star2: [awesome-kinesis](https://github.com/bbbbbrie/awesome-kinesis)
  - :cat: [HTTP Response Status Cats](https://httpcat.us)
  - :map: [Heat map of Pittsburgh city steps](https://412.brie.run/steps/steps)
